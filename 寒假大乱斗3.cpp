#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int help();
int menu();
int error();
int grade_1();
int grade_2();
int grade_3();
int main()
{
	int grade = 1;
	printf("===========口算生成器=============\n");
	printf("欢迎使用口算生成器老铁:\n\n");
	
	help();
	while(grade)
	{
		grade=menu();
		
		switch(grade)
		{
			case 1: grade_1(); break;
        case 2: grade_2(); break;
        case 3: grade_3(); break;
        case 4: help(); break;
        case 5: grade = 0; break;
        default: error(); grade = 1; break;
		}
	}
	 printf("程序结束, 欢迎下次使用\n");
    printf("任意键结束！！");
    
    return 0;
}

int help()
{
    printf("帮助信息：\n");
    printf("您需要输入命令代号来进行操作, 且\n");
    printf("一年级题目为不超过十位的加减法;\n");
    printf("二年级题目为不超过百位的乘除法;\n");
    printf("三年级题目为不超过百位的加减乘除混合题目.\n");
    printf("\n\n");
}

int menu()
{
    int a = 0;
    printf("操作列表:\n");
    printf("1)一年级    2)二年级    3)三年级\n");
    printf("4)帮助      5)退出程序\n");
    printf("请输入操作> ");
    scanf("%d", &a);
    printf("< 执行操作 :)\n");
    printf("\n\n");
    return a;
}

int error()
{
    printf("Error!!!出现错误\n");
    printf("错误操作指令\n");
    printf("\n\n");
}

int grade_1()
{
    printf("老铁一年级题目开始了\n");
    int sum = 0;
    printf("老铁快输入所需要的题目个数>");
    scanf("%d", &sum);
    printf("< 执行操作 :)\n\n");

    int a, b, c;
    srand((unsigned)time(NULL));

    printf("/*----- 一年级 -----*/\n\n");

    for (int i = 0; i < sum; i++)
    {
        a = rand() % 10;
        b = rand() % 10;
        c = rand() % 2;
        if (c == 0)
        {
            printf("%d + %d = ___\n", a, b);
        }
        else
        {
            printf("%d - %d = ___\n", a, b);
        }

    }
}


int grade_2()
{
    printf("现在是二年级题目了哦：\n");
    printf("输入所需要的题目个数");
    int sum=0; 
    scanf("%d", &sum);
    printf("< 执行操作 :)\n\n");

    int a, b, c;
    srand((unsigned)time(NULL));

    printf("/*----- 二年级 -----*/\n\n");

    for (int i = 0; i < sum; i++)
    {
        a = rand() % 100;
        b = rand() % 100;
        c = rand() % 2;
        if (c == 0)
        {
            printf("%d * %d = ___\n", a, b,a*b);
        }
        else
        {
            printf("%d / %d = ___\n", a, b,a/b);
        }
    }
}

	char op[4]={'+','-','*','/'};

int grade_3()
{
    printf("终于是三年级题目：\n");
    printf("老铁，快输入所需要的题目个数");
    int sum = 0;
    scanf("%d", &sum);
    printf("< 执行操作 :)\n\n");
    printf("/*----- 三年级 -----*/\n\n");
    for (int i = 0; i < sum; i++)
    {

    	printf("%2d %c %2d %c %2d = ___\n");
	}

}


 

