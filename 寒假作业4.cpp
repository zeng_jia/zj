#include <stdio.h>
#include <conio.h>
#include<stdlib.h>
#include<time.h>

int help();
int menu();
int error();
int grade_1();
int grade_2();
int grade_3();
double answer(double a, double b, double c, char d, char e);

int main()
{
    int grade = 1;

    printf("=================================jj口算生成器 ===================================================\n");
    printf("欢迎使用口算生成器哦，铁子 :)\n");
    printf("\n\n");

    help();
    while (grade)
    {
        grade = menu();

        switch (grade)
        {
        case 1: grade_1(); break;
        case 2: grade_2(); break;
        case 3: grade_3(); break;
        case 4: help(); break;
        case 5: grade = 0; break;
        default: error(); grade = 1; break;
        }
    }
    printf("程序结束, 欢迎下次使用\n");
    printf("任意键结束,hh……");

    _getch();
    return 0;
}

int help()
{
    printf("帮助信息：\n");
    printf("您需要输入命令代号来进行操作, 且\n");
    printf("一年级题目为不超过十位的加减法;\n");
    printf("二年级题目为不超过百位的乘除法;\n");
    printf("三年级题目为不超过百位的加减乘除混合题目.\n");
    printf("\n\n");
}

int menu()
{
    int a = 0;
    printf("操作列表:\n");
    printf("1)一年级    2)二年级    3)三年级\n");
    printf("4)帮助      5)退出程序\n");
    printf("请输入操作> ");
    scanf("%d", &a);
    printf("< 执行操作 :)\n");
    printf("\n\n");
    return a;
}

int error()
{
    printf("Error!!!报错\n");
    printf("错误操作, 重新输入，老铁\n");
    printf("\n\n");
}

int grade_1()
{
    printf("gkd,gkd,现在是一年级题目\n");
    printf("请输入你他妈所需要的题目个数>");
    int num = 0;
    scanf("%d", &num);
    printf("< 执行操作 :)\n\n");

    int a, b, c;
    srand((unsigned)time(NULL));

    printf("/*----- 一年级 -----*/\n\n");

    for (int i = 0; i < num; i++)
    {
        a = rand() % 10;
        b = rand() % 10;
        c = rand() % 2;
        if (c == 0)
        {
            printf("%d + %d = %d\n", a, b, a + b);
        }
        else
        {
            printf("%d - %d = %d\n", a, b, a - b);
        }
    }
}

int grade_2()
{
    printf("现在是二年级题目，孩子：\n");
    printf("你要几个题目");
    int num = 0;
    scanf("%d", &num);
    printf("< 执行操作 :)\n\n");

    double a, b, c;
    srand((unsigned)time(NULL));

    printf("/*----- 二年级 -----*/\n\n");

    for (int i = 0; i < num; i++)
    {
        a = rand() % 100;
        b = rand() % 100;
        c = rand() % 2;
        if (c == 0)
        {
            printf("%2g * %2g = %g\n", a, b, a * b);
        }
        else
        {
            printf("%2g / %2g = %g\n", a, b + 1, a / (b + 1));
        }
    }
}

int num_dividend()
{
    for (int i = 0; true; i++)
    {
        if (i = rand() % 100)
            return i;
    }
}

char four_arithmetic(int num)
{
    switch (num)
    {
    case 1: return '+';
    case 2: return '-';
    case 3: return '*';
    case 0: return '/';
    }
}
int grade_3()
{
    printf("卧槽，现在是三年级题目：\n");
    printf("请随便打一个数");
    int num = 0;
    scanf("%d", &num);
    printf("< 执行操作 :)\n\n");

    printf("/*----- 三年级 -----*/\n\n");

    double a = 0, b = 0, c = 0;
    char d, e;
    for (int i = 0; i < num; i++)
    {
        printf("%2g %c %2g %c %2g = %g\n"); 
    }
}

double answer(double a, double b, double c, char d, char e)
{
    double result = 0;
    if (d == '*' || d == '/')
    {
        switch (d)
        {
        case '*': result = a * b; break;
        case '/': result = a / b; break;
        }

        switch (e)
        {
        case '+': return result + c;
        case '-': return result - c;
        case '*': return result * c;
        case '/': return result / c;
        }
    }
    else if (e == '*' || e == '/')
    {
        switch (e)
        {
        case '*': result = b * c; break;
        case '/': result = b / c; break;
        }

        switch (d)
        {
        case '+': return a + result;
        case '-': return a - result;
        case '*': return a * result;
        case '/': return a / result;
        }
    }
    else
    {
        switch (d)
        {
        case '+': result = a + b; break;
        case '-': result = a - b; break;
        }

        switch (e)
        {
        case '+': return result + c;
        case '-': return result - c;
        }
    }
}
