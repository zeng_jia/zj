#include<stdio.h>
#include <stdlib.h>
int help();
int menu();
int error();
int grade_1();
int grade_2();
int grade_3();

int main()
{
	int grade = 1;
	printf("===========口算生成器=============\n");
	printf("欢迎使用口算生成器老铁:\n\n");
	
	help();
	 while (grade)
    {
        grade = menu();

        switch (grade)
        {
        case 1: grade_1(); break;
        case 2: grade_2(); break;
        case 3: grade_3(); break;
        case 4: help(); break;
        case 5: grade = 0; break;
        default: error(); grade = 1; break;
        }
    }
    printf("程序结束, 欢迎下次使用\n");
    printf("任意键结束！！");

    
    return 0;
}

int help()
{
    printf("帮助信息：\n");
    printf("您需要输入命令代号来进行操作, 且\n");
    printf("一年级题目为不超过十位的加减法;\n");
    printf("二年级题目为不超过百位的乘除法;\n");
    printf("三年级题目为不超过百位的加减乘除混合题目.\n");
    printf("\n\n");
}

int menu()
{
    int a = 0;
    printf("操作列表:\n");
    printf("1)一年级    2)二年级    3)三年级\n");
    printf("4)帮助      5)退出程序\n");
    printf("请输入操作> ");
    scanf("%d", &a);
    printf("< 执行操作 :)\n");
    printf("\n\n");
    return a;
}

int error()
{
    printf("Error!!!出现错误\n");
    printf("错误操作指令\n");
    printf("\n\n");
}

int grade_1()
{
    printf("现在是一年级题目：\n");
    printf("一年级题目你都会哦买噶\n");

    printf("\n\n");
}

int grade_2()
{
    printf("现在是二年级题目：\n");
    printf("二年级的题目成功，牛逼老铁\n");

    printf("\n\n");
}

int grade_3()
{
    printf("现在是三年级题目：\n");
    printf("三年级的题目成功，牛逼克拉斯\n");

    printf("\n\n");
}
